package vongboonmee.chayanis.lab5;
/**
*
* AndroidDevice is an abstract class to represent an Android device
* 
* @author Chayanis Vongboonmee
*
*/
public abstract class AndroidDevice {
    private final static String os = "Android";
    public abstract void usage();
    public static String getOS() {
        return os;
    }
}
