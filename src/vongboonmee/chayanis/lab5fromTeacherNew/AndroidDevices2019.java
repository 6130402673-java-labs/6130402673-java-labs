package vongboonmee.chayanis.lab5;
/**
 * 
 * AndroidDevices2019 is a program to test class AndroidDevice and its subclass.
 *  
 * @author Chayanis Vongboonmee
 *
 */
public class AndroidDevices2019 {
    public static void main(String[] args) {
        AndroidDevice ticwatchPro =
                new AndroidSmartWatch("Mobvoi", "TicWatch Pro", 8390);
        AndroidSmartWatch xiaomiMiBand3 = 
        new AndroidSmartWatch("Xiaomi", "Mi Band 3", 950);
        ticwatchPro.usage();
        System.out.println(ticwatchPro);
        System.out.println(xiaomiMiBand3);
    }
}