package vongboonmee.chayanis.lab5;

public class FavoriteDevices {
    public static double getPriceDiff(MobileDevice m1, MobileDevice m2) {
        return Math.abs(m1.getPrice() - m2.getPrice());
    }
    public static void main(String[] args) {
        MobileDevice s9 = new SamsungDevice("Galaxy S9", 23900, 163, 8.0);
        SamsungDevice note9 = new SamsungDevice("Galaxy Note 9", 25500, 201, 
                8.1);
        SamsungDevice a6 = new SamsungDevice("Galaxy A6", 6400, 162);
        System.out.println(s9.getModelName() + " has the price different from " + 
                note9.getModelName() + " as " +
                getPriceDiff(s9, note9) + " Baht.");
        System.out.println(note9.getModelName() + " has the price different from " + 
                a6.getModelName() + " as " +
                getPriceDiff(note9, a6) + " Baht.");
    }
 
}