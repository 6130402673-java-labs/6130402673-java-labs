package vongboonmee.chayanis.lab5;

public class TestPolymorphism {
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        AndroidSmartWatch ticwatchPro =
                new AndroidSmartWatch("Mobvoi", "TicWatch Pro", 8390);
        SamsungDevice note9 = new SamsungDevice("Galaxy Note 9", 25500, 201, 
                8.1);
        System.out.println(ticwatchPro);
        ticwatchPro.displayTime();
        System.out.println();
        System.out.println(note9);
        note9.displayTime();
    }
}