package vongboonmee.chayanis.lab5;
/**
 * 
 * This program is to illustrate the examples of how to create objects in 
 * class MobileDevice
 * 
 * @author  Chayanis Vongboonmee
 *
 */
public class InterestedMobileDevices {
    public static void main(String[] args) {
        MobileDevice galaxyNote9 = new MobileDevice("Galaxy Note 9", "Android", 25500, 201);
        MobileDevice iPadGen6 = new MobileDevice("Apple iPad Mini 3", "iOS",
                11500);
        System.out.println(galaxyNote9);
        System.out.println(iPadGen6);
        iPadGen6.setPrice(11000);
        System.out.println(iPadGen6.getModelName() + " has new price as "
                + iPadGen6.getPrice() + " Baht.");
        System.out.println(galaxyNote9.getModelName() + " has " + " weight as "
                + galaxyNote9.getWeight() + " grams.");
    }
}
