package vongboonmee.chayanis.lab5;
import java.io.Serializable;

/**
 *
 * MobileDevice is to model a mobile device object with 
 * attributes modelName, os, price,  and weight 
 * 
 * @author  Chayanis Vongboonmee
 *
 */
public class  MobileDevice implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3929837871841003508L;
	private String modelName, os;
    private int price, weight;
 
    /**
     * Constructor ModelDevice to create a mobile device object 
     * with the information about model name, os, and price 
     * 
     * @param modelName
     * @param os
     * @param price
     */
    public MobileDevice(String modelName, String os, int price) {
        this.modelName = modelName;
        this.os = os;
        this.price = price;
    }
     
    /**
     * Constructor ModelDevice to create a mobile device object 
     * with the information about model name, os, price, and weight
     * 
     * @param modelName
     * @param os
     * @param price
     * @param weight
     */
    public MobileDevice(String modelName, String os, int price,
            int weight) {
        this(modelName, os, price);
        this.weight = weight;
    }
     

    
    
    

	/**
     * Method getSpec()
     * 
     * @return specification of this mobile device object
     */
    protected String getSpec() {
        return "Model name:" + modelName  + ", OS: " + os  + ", Price:"
                + price + " Baht, Weight:"
                + weight + " g";
    }
     
    /**
     * Method toString() is to display the message when a user 
     * prints this object
     * 
     * @return the information about this mobile device object
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "[" + getSpec() +  "]\n";
    }
 
    /**
     * Method getModelName() is to return the model name of this object
     * 
     * @return the model name of this object
     */
    public String getModelName() {
        return modelName;
    }
 
    public void setModelName(String modelName) {
        this.modelName = modelName;
    }
 
    public String getOs() {
        return os;
    }
 
    public void setOs(String os) {
        this.os = os;
    }
 
    public int getPrice() {
        return price;
    }
 
    public void setPrice(int price) {
        this.price = price;
    }
 
 
    public int getWeight() {
        return weight;
    }
 
    public void setWeight(int weight) {
        this.weight = weight;
    }
 
}