package vongboonmee.chayanis.lab5;

/**
 * 
 * SamsungDevice is to a subclass of MobileDevice and it is
 * to model a device object with brand "Samsung"
 * 
 * 
 * @author  Chayanis Vongboonmee
 *
 */
public class SamsungDevice extends MobileDevice {
     
    /**
	 * 
	 */
	private static final long serialVersionUID = -811959905870773656L;
	private double androidVersion;
    private static String brand = "Samsung";
  
     
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        if (androidVersion == 0.0) 
            return super.toString();
        else
            return this.getClass().getSimpleName() +"[" + getSpec()  + ", Android version:" + androidVersion
                        + "]";
    }
 
    public double getAndroidVersion() {
        return androidVersion;
    }
 
    public void setAndroidVersion(double androidVersion) {
        this.androidVersion = androidVersion;
    }
 
    public static String getBrand() {
        return brand;
    }
 
    public static void setBrand(String brand) {
        SamsungDevice.brand = brand;
    }
 
    public SamsungDevice(String modelName, int price,
            double androidVersion) {
        super(modelName, "Android", price);
        this.androidVersion = androidVersion;
        // TODO Auto-generated constructor stub
    }
     
    public SamsungDevice(String modelName, int price,
            int weight, double androidVersion) {
        super(modelName, "Android", price, weight);
        this.androidVersion = androidVersion;
        // TODO Auto-generated constructor stub
    }
     
    public void displayTime() {
        System.out.println("Display times in both using a digital "
                + "format and using an analog watch");
    }
     
}