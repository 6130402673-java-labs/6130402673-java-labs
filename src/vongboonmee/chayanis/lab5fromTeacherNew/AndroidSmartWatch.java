package vongboonmee.chayanis.lab5;
/**
*
* AndroidSmartWatch is a subclass of an abstract class AndroidDevice
* 
* @author  Chayanis Vongboonmee
*
*/
public class AndroidSmartWatch extends AndroidDevice {
   private String brandName, modelName;
   private int price;
   public AndroidSmartWatch(String brandName, String modelName, int price) {
       this.brandName = brandName;
       this.modelName = modelName;
       this.price = price;
   }
   public void usage() {
       System.out.println(this.getClass().getSimpleName() 
               + " Usage: Show time, date, your heart rate, and your step count");
   }
   protected String getSpec() {
       return "Brand name:" + brandName  + ", Model name: " + modelName + 
               ", Price:" + price + " Baht";
   }
   public String toString() {
       return this.getClass().getSimpleName() + "[" + getSpec() + "]";
   }
   public void displayTime() {
       System.out.println("Display time only using a digital format");
   }
}