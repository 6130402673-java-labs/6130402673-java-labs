package vongboonmee.chayanis.lab7;

import java.awt.Color;
import java.awt.Font;

import javax.swing.SwingUtilities;

public class MobileDeviceFormV5 extends MobileDeviceFormV4 {

	private static final long serialVersionUID = 4856657315448982032L;

	protected Font font14Plain = new Font("Serif", Font.PLAIN, 14);
	protected Font font14Bold = new Font("Serif", Font.BOLD, 14);

	public MobileDeviceFormV5(String title) {
		super(title);
	}

	protected void initComponents() {
		super.initComponents();
		featuresLabel.setFont(font14Plain);
		reviewLabel.setFont(font14Plain);
		typeLabel.setFont(font14Plain);
		bNameLabel.setFont(font14Plain);
		mNameLabel.setFont(font14Plain);
		weightLabel.setFont(font14Plain);
		priceLabel.setFont(font14Plain);
		osLabel.setFont(font14Plain);

		bNameTxtField.setFont(font14Bold);
		mNameTxtField.setFont(font14Bold);
		weightTxtField.setFont(font14Bold);
		priceTxtField.setFont(font14Bold);
		reviewTxtArea.setFont(font14Bold);

		bNameTxtField.setText("Samsung");
		mNameTxtField.setText("Galaxy Note 9");
		weightTxtField.setText("201");
		priceTxtField.setText("25500"); 

		cancelButton.setForeground(Color.RED);
		okButton.setForeground(Color.BLUE);
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV5 mobileDeviceForm5 = new MobileDeviceFormV5("Mobile Device Form V5");
		mobileDeviceForm5.addComponents();
		mobileDeviceForm5.addMenus();
		mobileDeviceForm5.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}