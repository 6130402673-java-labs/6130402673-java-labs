package vongboonmee.chayanis.lab6;

import java.awt.BorderLayout;
import java.awt.GridLayout;
 
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
 
public class MobileDeviceFormV2 extends MobileDeviceFormV1 {
    /**
     * 
     */
    private static final long serialVersionUID = 3694909354550437224L;
 
    protected JTextArea reviewTxtArea;
    protected JScrollPane reviewScrollPane;
    protected JPanel reviewPanel, contentPanel, typePanel;
    protected JLabel reviewLabel, typeLabel;
    protected JComboBox<String> typeBox;
    protected String[] typesValues = {"Phone", "Tablet", "Smart TV"};
     
    public final static int NUM_TXTAREA_ROWS = 2;
    public final static int NUM_TXTAREA_COLS = 35;
     
    public MobileDeviceFormV2(String title) {
        super(title);
    }
     
    protected void initComponents() {
        super.initComponents();
        reviewTxtArea = new JTextArea(NUM_TXTAREA_ROWS, NUM_TXTAREA_COLS);
        reviewTxtArea.setLineWrap(true);
        reviewTxtArea.setWrapStyleWord(true);
        reviewTxtArea.setText("Bigger than previous Note phones in every way,");
        reviewTxtArea.append(" the Samsung Galaxy Note 9");
        reviewTxtArea.append(" has a larger 6.4-inch screen, heftier 4,000mAh "
                + "battery, and a massive 1TB of storage option. ");
        reviewScrollPane = new JScrollPane(reviewTxtArea);
        reviewPanel = new JPanel(new BorderLayout());
        contentPanel = new JPanel(new BorderLayout());
        typePanel = new JPanel(new GridLayout(1,2));
        reviewLabel = new JLabel("Review:");
        typeLabel = new JLabel("Type:");
        typeBox = new JComboBox<String>(typesValues);
    }
     
    protected void addComponents() {
        super.addComponents();
        reviewPanel.add(reviewLabel, BorderLayout.NORTH);
        reviewPanel.add(reviewScrollPane, BorderLayout.SOUTH);
        typePanel.add(typeLabel);
        typePanel.add(typeBox);
        contentPanel.add(textsPanel, BorderLayout.NORTH);
        contentPanel.add(typePanel, BorderLayout.CENTER);
        contentPanel.add(reviewPanel, BorderLayout.SOUTH);
        overallPanel.add(contentPanel, BorderLayout.NORTH);
    }
    public static void createAndShowGUI(){
        MobileDeviceFormV2 mobileDeviceForm2 = 
                new MobileDeviceFormV2("Mobile Device Form V2");
        mobileDeviceForm2.addComponents();
        mobileDeviceForm2.setFrameFeatures();
    }
     
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}