package vongboonmee.chayanis.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV3 extends MyCanvasV2 {
	static final long serialVersionUID = 1L;
	MyBall ballOne = new MyBall(0, 0);
	MyBall ballTwo = new MyBall(WIDTH - 30, 0);
	MyBall ballThree = new MyBall(0, HEIGHT - 30);
	MyBall ballFour = new MyBall(WIDTH - 30, HEIGHT - 30);

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.WHITE);
		g2d.fill(ballOne);
		g2d.fill(ballTwo);
		g2d.fill(ballThree);
		g2d.fill(ballFour);
		g2d.setStroke(new BasicStroke(5));
		for (int i = 0; i < 10; i++) {
			MyBrick brick = new MyBrick(MyBrick.brickWidth * i, (HEIGHT / 2) - (MyBrick.brickHeight / 2));
			g2d.setColor(Color.WHITE);
			g2d.fill(brick);
			g2d.setColor(Color.BLACK);
			g2d.draw(brick);

		}

	}

}
