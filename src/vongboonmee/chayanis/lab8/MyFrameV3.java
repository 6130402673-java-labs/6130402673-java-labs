package vongboonmee.chayanis.lab8;

import javax.swing.SwingUtilities;

public class MyFrameV3 extends MyFrameV2 {
	static final long serialVersionUID = 1L;

	public MyFrameV3(String string) {
		super(string);
	}

	protected void addComponents() {
		add(new MyCanvasV3());
	}

	public static void createAndShowGUI() {
		MyFrameV3 msw = new MyFrameV3("My Frame V3");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}

		});
	}
}
