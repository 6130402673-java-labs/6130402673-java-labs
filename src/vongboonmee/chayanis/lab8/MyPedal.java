package vongboonmee.chayanis.lab8;

import java.awt.geom.Rectangle2D;

public class MyPedal extends Rectangle2D.Double {
	static final long serialVersionUID = -3738766094157952102L;
	protected final static int brickWidth = 100;
	protected final static int brickHeight = 10;

	public MyPedal(int x, int y) {
		super(x, y, brickWidth, brickHeight);
	}
}
