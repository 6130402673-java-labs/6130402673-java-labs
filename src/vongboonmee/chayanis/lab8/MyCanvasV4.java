package vongboonmee.chayanis.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV4 extends MyCanvasV3 {
	static final long serialVersionUID = 1L;
	Color[] color = { Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.CYAN, Color.BLUE, Color.MAGENTA };
	MyBrick myBrick = new MyBrick(0, 0);
	MyPedal myPedal = new MyPedal(350, 590);
	MyBall myBall = new MyBall(385, 560);

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setStroke(new BasicStroke(3));
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 7; j++) {
				myBrick.x = i * MyBrick.brickWidth;
				myBrick.y = HEIGHT / 6 + j * MyBrick.brickHeight;
				g2d.setColor(color[j]);
				g2d.fill(myBrick);
				g2d.setColor(Color.BLACK);
				g2d.draw(myBrick);
			}
		}
		g2d.setColor(Color.GRAY);
		g2d.fill(myPedal);
		g2d.setColor(Color.WHITE);
		g2d.fill(myBall);
	}
}
