package vongboonmee.chayanis.lab8;

import javax.swing.SwingUtilities;

public class MyFrameV2 extends MyFrame {
	static final long serialVersionUID = -7557992723994671816L;

	public MyFrameV2(String string) {
		super(string);
	}

	protected void addComponents() {
		add(new MyCanvasV2());
	}

	public static void createAndShowGUI() {
		MyFrameV2 msw = new MyFrameV2("My Frame V2");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}

		});
	}
}
