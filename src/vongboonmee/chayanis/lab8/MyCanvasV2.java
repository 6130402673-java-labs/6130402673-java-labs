package vongboonmee.chayanis.lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV2 extends MyCanvas {
	static final long serialVersionUID = 8273642122651349213L;
	MyBrick myBrick = new MyBrick(WIDTH / 2 - 40, 0);
	MyPedal myPedal = new MyPedal(WIDTH / 2 - 50, HEIGHT - 10);
	MyBall myBall = new MyBall(WIDTH / 2 - 15, HEIGHT / 2 - 15);

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.WHITE);
		g2d.fill(myBall);
		g2d.fill(myBrick);
		g2d.fill(myPedal);
		g2d.drawLine(0, HEIGHT / 2, WIDTH, HEIGHT / 2);
		g2d.drawLine(WIDTH / 2, 0, WIDTH / 2, HEIGHT);

	}
}