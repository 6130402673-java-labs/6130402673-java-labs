package vongboonmee.chayanis.lab8;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;

public class MyCanvas extends JPanel {
	protected static final long serialVersionUID = 1425088587206134901L;
	protected final int WIDTH = 800;
	protected final int HEIGHT = 600;

	public void setPreferredSize() {
		setSize(WIDTH, HEIGHT);
	}

	public MyCanvas() {
		super();
		this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		this.setBackground(Color.BLACK);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.WHITE);
		Ellipse2D face = new Ellipse2D.Double(WIDTH / 2 - 150, HEIGHT / 2 - 150, 300, 300);
		g2d.draw(face);
		Ellipse2D eyeLeft = new Ellipse2D.Double(WIDTH / 2 - 80, HEIGHT / 2 - 60, 30, 60);
		g2d.draw(eyeLeft);
		Ellipse2D eyeRight = new Ellipse2D.Double(WIDTH / 2 + 50, HEIGHT / 2 - 60, 30, 60);
		g2d.draw(eyeRight);
		Rectangle2D mouth = new Rectangle2D.Double(WIDTH / 2 - 50, HEIGHT / 2 + 60, 100, 10);
		g2d.draw(mouth);
		g2d.fill(eyeLeft);
		g2d.fill(eyeRight);
		g2d.fill(mouth);
	}
	
}
