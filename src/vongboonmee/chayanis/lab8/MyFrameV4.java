package vongboonmee.chayanis.lab8;

import javax.swing.SwingUtilities;

public class MyFrameV4 extends MyFrameV3 {

	static final long serialVersionUID = 1L;

	public MyFrameV4(String string) {
		super(string);
		
	}
	protected void addComponents() {
		add(new MyCanvasV4());
	}

	public static void createAndShowGUI() {
		MyFrameV4 msw = new MyFrameV4("My Frame V4");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}

		});
	}
}
