package vongboonmee.chayanis.lab8;

import java.awt.geom.Rectangle2D;

public class MyBrick extends Rectangle2D.Double {
	static final long serialVersionUID = -3738766094157952102L;
	protected final static int brickWidth = 80;
	protected final static int brickHeight = 20;

	public MyBrick(int x, int y) {
		super(x, y, brickWidth, brickHeight);
	}

}
