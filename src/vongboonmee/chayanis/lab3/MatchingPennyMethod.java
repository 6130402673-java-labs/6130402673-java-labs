package vongboonmee.chayanis.lab3;
import java.util.Scanner;
/**The program reads in a choice (Head or Tail) from a user then compares 
 *that choice with a randomly generated choice from a computer. If the choices are the same,
 * the user wins. If the choices are different, the computer wins.
 * 
 * author : Chayanis Vongboonmee
 * section : 2
 * ID : 613040267-3
 * Date: 4/2/2019
 */
public class MatchingPennyMethod {
	static String acceptInput() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter head or tail : ");
		String yourAnswer = scanner.next();
		yourAnswer = yourAnswer.toLowerCase();
		if (yourAnswer.equals("tail")) {
			System.out.println("you play tail.");
		} else if (yourAnswer.equals("head")) {
			System.out.println("you play head.");
		} else if (yourAnswer.equals("exit.")) {
			System.out.println("Good bye.");
			scanner.close();
			System.exit(0);
		} else {
			System.err.println("Incorrect input. head or tail.");
		}
		return yourAnswer;
	}
	static String genComChoice() {
		int max = 1, min = 0;
		int random = min + (int) (Math.random() * ((max - min) + 1));
		String comPlays = null;
		if (random == 1) {
			System.out.println("Computer plays tail.");
		} else {
			System.out.println("Computer plays head.");
		}
		return comPlays;
	}
	static void displayWiner(String yourAnswer, String comPlays) {
		if (yourAnswer.equals(comPlays)) {
			System.out.println("You win.");
		} else {
			System.out.println("Computer win.");
		}
	}
	public static void main(String[] args) {
		while (true) {
			String answer = acceptInput();
			String comAnswer = genComChoice();
			displayWiner(answer, comAnswer);
		}
	}
}