package vongboonmee.chayanis.lab3;
import java.util.Arrays;
/**This program shows numbers of your input
 * Display :
 * shows maximum , minimum, average and
 * standard
 * author : Chayanis Vongboonmee
 * section : 2
 * ID : 613040267-3
 * Date: 4/2/2019
 */
public class BasicStat {
	public static int numInput;
	public static double num[];
	public static void main(String[] args) {
		acceptInput(args);
		System.out.println(displayStats(args));
	}
	public static double[] acceptInput(String[] args) {
		int firstNumber = Integer.parseInt(args[0]);
		if (firstNumber != (args.length - 1)) {
			System.err.println("<BasicStat> <numNumbers> <numbers>...");
		} else {
			numInput = firstNumber;
			num = new double[numInput + 1];
			for (int i = 0; i <= numInput; i++) {
				double args_double = Double.parseDouble(args[i]);
				num[i] = args_double;
			}
		}
		return num;
	}
	public static String displayStats(String[] args) {
		double[] num1 = new double[numInput];
		for (int i = 1; i <= numInput; i++) {
			num1[i - 1] = num[i];
		}
		Arrays.sort(num1);
		double max = num1[numInput - 1];
		double min = num1[0];
		double sum = 0;
		for (int i = 0; i < num1.length; i++) {
			sum += num1[i];
		}
		double avg = sum / numInput;
		double sum_sd = 0;
		for (int i = 0; i < num1.length; i++) {
			sum_sd += Math.pow((num1[i] - avg), 2);
		}
		double standard_dev = Math.sqrt(sum_sd / numInput);
		String display_result = ("Max is " + max + " Min is " + min + "\n" + "Average is " + avg + "\n"
				+ "Standard Deviation is " + standard_dev);
		return display_result;
	}
}