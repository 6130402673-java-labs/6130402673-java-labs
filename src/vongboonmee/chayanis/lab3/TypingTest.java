package vongboonmee.chayanis.lab3;
import java.util.Scanner;
/**This program shows and randoms 7 colors of rainbow that user input this.
 * Display :
 * This program shows time when user has finished and
 * shows taxts when the user type faster than average,
 * shows taxts when the user type slower than average
 *
 * author : Chayanis Vongboonmee
 * section : 2
 * ID : 613040267-3
 * Date: 4/2/2019
 *
 */
public class TypingTest {
	static String randomColor() {
		String[] rainbow = { "RED", "ORANGE", "YELLOW", "GREEN", "BLUE", "INDIGO", "VIOLET" };
		String rainbowRandom = "";
		for (int i = 0; i < 8; i++) {
			int randomNumber = (int) (Math.random() * 7);
			if (i == 7) {
				rainbowRandom += rainbow[randomNumber];
			} else {
				rainbowRandom += rainbow[randomNumber] + " ";
			}
		}
		return rainbowRandom;
	}
	public static float runTime;
	public static void main(String[] args) {
		String random = randomColor();
		System.out.println(random);
		double startTime = System.currentTimeMillis();
		while (true) {
			System.out.print("Type your answer : ");
			Scanner scanner = new Scanner(System.in);
			String userWrite = scanner.nextLine();
			if (userWrite.equalsIgnoreCase(random)) {
				runTime = (float) ((System.currentTimeMillis() - startTime) / 1000);
				scanner.close();
				break;
			} else {
				continue;
			}
		}
		System.out.println("Your time is " + runTime);
		if (runTime <= 12) {
			System.out.println("You type faster than average person");
		} else {
			System.out.println("You type slower than average person");
		}
		System.exit(0);
	}
}