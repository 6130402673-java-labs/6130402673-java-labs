package vongboonmee.chayanis.lab3;
import java.util.Scanner;
/**The program reads in a choice (Head or Tail) from a user then compares 
 *that choice with a randomly generated choice from a computer. If the choices are the same,
 * the user wins. If the choices are different, the computer wins.
 * 
 * author : Chayanis Vongboonmee
 * section : 2
 * ID : 613040267-3
 * Date: 4/2/2019
 */
public class MatchingPenny {
	public static void main(String[] args) {
		while (true) {
			Scanner scanner = new Scanner(System.in);
			System.out.println("Enter head or tail : ");
			String yourAnswer = scanner.next();
			int max = 1, min = 0;
			int random = min + (int) (Math.random() * ((max - min) + 1));
			int penny;
			if (yourAnswer.toLowerCase().contains("tail") || yourAnswer.toLowerCase().contains("head")) {
				if (yourAnswer.toLowerCase().contains("tail")) {
					penny = 0;
					System.out.println("you play tail");
				} else {
					penny = 1;
					System.out.println("you play head");
				}
				if (random == 0) {
					System.out.println("computer plays tail");
				} else {
					System.out.println("computer plays head");
				}
				if (random == penny) {
					System.out.println("You win.");
				} else {
					System.out.println("Computer win.");
				}
			} else if ((yourAnswer.toLowerCase().contains("exit"))) {
				System.out.println("Good bye");
				scanner.close();
				System.exit(0);
			} else {
				System.err.println("Incorrect input. head or tail.");
			}
		}
	}
}