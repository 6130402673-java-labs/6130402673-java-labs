package vongboonmee.chayanis.lab4;
/**
 * This java program was create to check if the code from Automobile were correct or not
 * 
 * @author Chayanis Vongboonmee
 * ID: 613040267-3
 * Section: 2
 * Date: February 11, 2019
 */

public class TestDrive extends Automobile {
	public static void main(String[] args) {
		Automobile car1 = new Automobile(100, 0, 200, 0, "Sport car", Color.RED);
		Automobile car2 = new Automobile(100, 0, 100, 0, "Automoblie", Color.WHITE);
		System.out.println("Car1 's max speed is " + car1.getMaxSpeed());
		System.out.println("Car2 's max speed is " + car2.getMaxSpeed());
		car1.setMaxSpeed(280);
		System.out.println("Car1 's max speed has increased to " + car1.getMaxSpeed());
		System.out.println("There are " + Automobile.getNumberOfAutomobile() + " automobile");
		Automobile car3 = new Automobile();
		System.out.println("Now There are " + Automobile.getNumberOfAutomobile() + " automobile");
		System.out.println(car1);
		System.out.println(car3);
	}
}
