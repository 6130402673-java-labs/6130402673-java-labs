package vongboonmee.chayanis.lab4;
/**
 * This java program will create a car with all the information needed
 * 
 * @author Chayanis Vongboonmee
 * ID: 613040267-3
 * Section: 2
 * Date: February 11, 2019
 * 
 **/

public class Automobile {
	private int gasoline;
	private int speed;
	private int maxSpeed;
	private int acceleration;
	private String model;
	private static int numberOfAutomobile = 0;
	private Color color;
	enum Color {
		RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, VIOLET, WHITE, BLACK
	}
	Automobile(int gasoline, int speed, int maxSpeed, int acceleration, String model, Color color) {
		this.gasoline = gasoline;
		this.speed = speed;
		this.maxSpeed = maxSpeed;
		this.acceleration = acceleration;
		this.model = model;
		this.color = color;
		numberOfAutomobile++;	
	}
	Automobile() {
		this.gasoline = 0;
		this.speed = 0;
		this.maxSpeed = 160;
		this.acceleration = 0;
		this.model = "Automobile";
		this.color = Color.WHITE;
		numberOfAutomobile++;
	}
	public void setThisSpeed(int speed) {
		this.speed = speed;
		}
	public int getGasoline() {
		return gasoline;
	}
	public void setGasoline(int gasoline) {
		this.gasoline = gasoline;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public int getMaxSpeed() {
		return maxSpeed;
	}
	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public int getAcceleration() {
		return acceleration;
	}
	public void setAcceleration(int acceleration) {
		this.acceleration = acceleration;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public static int getNumberOfAutomobile() {
		return numberOfAutomobile;
	}
	public  void setNumberOfAutomobile(int numberOfAutomobile) {
		Automobile.numberOfAutomobile = numberOfAutomobile;
	}
	public Color getColor() {
		return color;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	
	@Override
	public String toString() {
		return "Automobile [gasoline=" + gasoline + ", speed=" + speed + ", maxSpeed=" + maxSpeed + ", acceleration="
				+ acceleration + ", model=" + model + ", color=" + color + "]";
	}
	public static void main(String[] args) {
	}
}
