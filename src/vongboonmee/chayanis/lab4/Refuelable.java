package vongboonmee.chayanis.lab4;
/*
 * @author Chayanis Vongboonmee
 * ID: 613040267-3
 * Section: 2
 * Date: February 11, 2019
 *
 */
public interface Refuelable {
	public void refuel();
}
