package vongboonmee.chayanis.lab4;
/**
 * This java program was create to check if the code from 
 * HondaAuto and ToyotaAuto were correct or not.
 * 
 * @author Chayanis Vongboonmee
 * ID: 613040267-3
 * Section: 2
 * Date: February 11, 2019
 */

public class TestDrive2 {
	public static void isFaster(Automobile car1, Automobile car2) {
		if (car1.getSpeed() > car2.getSpeed()) {
			System.out.println( car1.getModel() + " is faster than " + car2.getModel());	
		}
		else {
			System.out.println(car1.getModel() + " is Not faster than " + car2.getModel());
			
		}
		
	}
	public static void main(String[] atgs) {
		ToyotaAuto car1 = new ToyotaAuto(200, 10, "Vios");
		HondaAuto car2 = new HondaAuto(220, 8, "City");
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.accelerate();
		car2.accelerate();
		car2.accelerate();
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.brake();
		car1.brake();
		car2.brake();
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.refuel();
		car2.refuel();
		System.out.println(car1);
		System.out.println(car2);
		isFaster(car1, car2);
		isFaster(car2, car1);
	}
}
