package vongboonmee.chayanis.lab4;
/**
 * This java program was extends from Automobile. 
 * This program will create a car like Automobile but by it's own type (ToyotaAuto)
 * 
 * @author Chayanis Vongboonmee
 * ID: 613040267-3
 * Section: 2
 * Date: February 11, 2019
 *
 */
public class ToyotaAuto extends Automobile implements Moveable, Refuelable {
	ToyotaAuto(int maxSpeed, int acceleration, String model) {
		this.setMaxSpeed(maxSpeed);
		this.setAcceleration(acceleration);
		this.setModel(model);
		this.setGasoline(100);
	}

	public void refuel() {
		this.setGasoline(100);
		System.out.println(this.getModel() + "refuels");
	}

	public String toString() {
		return (this.getModel() + " gas: " + this.getGasoline() + " speed:" + this.getSpeed() + "max speed:"
				+ this.getMaxSpeed() + "acceleration:" + this.getAcceleration());
	}

	public void accelerate() {
		if (this.getSpeed() + this.getMaxSpeed() <= this.getMaxSpeed()) {
			this.setSpeed(this.getSpeed() + this.getAcceleration());
			this.setGasoline(getGasoline() - 15);
		} else {
			this.setMaxSpeed(this.getMaxSpeed());
			this.setGasoline(getGasoline() - 15);
		}
		System.out.println(this.getModel() + "acceleration");
	}

	public void brake() {
		if (this.getSpeed() - this.getAcceleration() >= 0) {
			this.setSpeed(this.getSpeed() - this.getAcceleration());
			this.setGasoline(getGasoline() - 15);
		} else {
			this.setSpeed(0);
			this.setGasoline(getGasoline() - 15);
		}
		System.out.println(this.getModel() + "breaks");
	}

	public void setSpeed(int speed) {
		if (speed < 0) {
			this.setThisSpeed(0);
		} else if (speed > this.getMaxSpeed()) {
			this.setThisSpeed(this.getMaxSpeed());
			;
		} else {
			this.setThisSpeed(speed);
		}
	}
	
	public static void main(String[] args) {
	}
	
}
