/**This Java program that accepts five arguments: 5 numbers
* the program is in the format:
* your numbers to float-type and the program will be create list to keep number of float.
* Then,this program uses "Arrays.sort" to sort your numbers and shows sort of your numbers. 
* 
*  Author: Chayanis Vongboonmee
* ID: 613040267-3
* Section: 2
* Date: January 21, 2019 
* 
**/
package vongboonmee.chayanis.lab2;

import java.util.Arrays;

public class SortNumber {
	public static void main(String[] args) {
		String one = args[0];
		String two = args[1];
		String three = args[2];
		String four = args[3];
		String five = args[4];
		float onetoFloat = Float.parseFloat(one);
		float twotoFloat = Float.parseFloat(two);
		float threetoFloat = Float.parseFloat(three);
		float fourtoFloat = Float.parseFloat(four);
		float fivetoFloat = Float.parseFloat(five);
		float[] numbers = { onetoFloat, twotoFloat, threetoFloat, fourtoFloat, fivetoFloat };
		Arrays.sort(numbers);
		for (int i = 0; i < numbers.length; i++) {
			System.out.print(numbers[i] + " ");
		}
	}

}
