/** This is java programs that show My dataTypes. For example, my name, my ID, 
* the first letter Of my first name, My ID in octal to the integer, 
* My ID in hexadecimal to the integer, My ID in long-type to the integer,
* My ID in float-type and My ID in double-type.
* 
* Author: Chayanis Vongboonmee
* ID: 613040267-3
* Section: 2
* Date: January 21, 2019 
* 
**/
package vongboonmee.chayanis.lab2;

public class DataType {
	public static void main(String[] args) {
		String name = "Chayanis Vongboonmee";
		String id = "6130402673";
		char fistletteroffistname = name.charAt(0);
		boolean name1 = true;
		int myIDInOctal = 0111;
		int myIDInhex = 0x49;
		long myIDInlong = 73L;
		float myIDInfloat = 73.61f;
		double myIDIndouble = 73.61;
		System.out.println("My name is " + name);
		System.out.println("My student ID is " + id);
		System.out.println(fistletteroffistname + " " + name1 + " " + myIDInOctal + " " + myIDInhex);
		System.out.println(myIDInlong + " " + myIDInfloat + " " + myIDIndouble);
	}
}
