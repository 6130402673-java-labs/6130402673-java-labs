/** This Java program that accepts one arguments: your school or your university or your college.
* the program is in the format:
* word in input by user. User must be use " " to input your university.
* This program will be contain the key word in <schoolName> to 
*compare the <university> and <college>, if it true the program showing <schoolName> 
*is a university or <schoolName> is a college.Moreover the program will be show 
*<schoolName> is neither a university nor a college.
* 
* Author: Chayanis Vongboonmee
* ID: 613040267-3
* Section: 2
* Date: January 21, 2019
*  
**/
package vongboonmee.chayanis.lab2;

public class StringAPI {
	public static void main(String[] args) {
		String schoolName = args[0];
		String university = "University";
		String college = "College";
		boolean checkUniversity = schoolName.contains(university);
		boolean checkCollege = schoolName.contains(college);
		if (checkUniversity == true ) {
			System.out.println(schoolName + " is a university.");	
		} else if (checkCollege == true ) {
			System.out.println(schoolName + " is a college.");	
		} else {
			System.out.println(schoolName + " is neither a university nor a college.");	
		}
	}
}
