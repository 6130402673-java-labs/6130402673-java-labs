/**This Java program that accepts four arguments: Number of 1,000 Baht banks ,
*  Number of 500 Baht banks, Number of 100 Baht banks ,Number of 20 Baht banks.
*  and show all of amount money.
* 
* Author: Chayanis Vongboonmee
* ID: 613040267-3
* Section: 2
* Date: January 21, 2019 
* 
**/
package vongboonmee.chayanis.lab2;

public class ComputeMoney {
	public static void main(String[] args) {
		if (args.length != 4) {
			System.err.println("Usage: ComputeMoney <1,000 Baht> <500 Baht> <100 Baht> <20 Baht>");
		} else {
			String onethousand = args[0];
			String fivehundred = args[1];
			String onehundred = args[2];
			String twenty = args[3];
			int onethousandbaht = Integer.parseInt(onethousand);
			int fivehundredbaht = Integer.parseInt(fivehundred);
			int onehundredbaht = Integer.parseInt(onehundred);
			int twentybaht = Integer.parseInt(twenty);
			int sum = (onethousandbaht * 1000) + (fivehundredbaht * 500) + (onehundredbaht * 100) + (twentybaht * 20);
			System.out.println("Total is " + sum + "Baht.");
		}
	}
}
