package vongboonmee.chayanis.lab10;

import java.awt.event.*;
import java.io.File;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import javafx.beans.value.*;

import java.awt.*;


public class MobileDeviceFormV9 extends MobileDeviceFormV8 implements ActionListener {

	private static final long serialVersionUID = 1776444630286572786L;
	JFileChooser fc = new JFileChooser();

	public MobileDeviceFormV9(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	protected void addMenus() {
		super.addMenus();

		openMI.addActionListener(this);
		saveMI.addActionListener(this);
		exitMI.addActionListener(this);
		redMI.addActionListener(this);
		greenMI.addActionListener(this);
		blueMI.addActionListener(this);

		customMenu.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				Color newColor = JColorChooser.showDialog(null, "Chooser Color ", reviewTxtArea.getBackground());
				reviewTxtArea.setBackground(newColor);
			}

		});

	}

	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);

		if (event.getSource() == openMI) {

			int returnVal = fc.showOpenDialog(MobileDeviceFormV9.this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				JOptionPane.showMessageDialog(null, "Opening file " + file);

			} else if (returnVal == JFileChooser.CANCEL_OPTION) {
				JOptionPane.showMessageDialog(null, "Open command cancelled by user");

			}
		} else if (event.getSource() == saveMI) {

			int returnVal = fc.showSaveDialog(MobileDeviceFormV9.this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				JOptionPane.showMessageDialog(null, "Saving file " + file);

			} else if (returnVal == JFileChooser.CANCEL_OPTION) {
				JOptionPane.showMessageDialog(null, "Save command cancelled by user");

			}
		} else if (event.getSource() == exitMI) {
			System.exit(0);

		} else if (event.getSource() == redMI) {

			reviewTxtArea.setBackground(Color.RED);

		} else if (event.getSource() == greenMI) {

			reviewTxtArea.setBackground(Color.GREEN);

		} else if (event.getSource() == blueMI) {

			reviewTxtArea.setBackground(Color.BLUE);

		}

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV9 MobileDeviceFormV9 = new MobileDeviceFormV9("Mobile Device Form V9");
		MobileDeviceFormV9.addComponents();
		MobileDeviceFormV9.addMenus();
		MobileDeviceFormV9.setFrameFeatures();

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
