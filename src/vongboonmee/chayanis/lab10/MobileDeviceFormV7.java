package vongboonmee.chayanis.lab10;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import vongboonmee.chayanis.lab7.MobileDeviceFormV6;

public class MobileDeviceFormV7 extends MobileDeviceFormV6 implements ActionListener, ItemListener {
	private static final long serialVersionUID = 1796217147462470084L;
	protected String detailandOSMessage, typeMessage, featuresMessage, reviewMessage;
	protected ListSelectionModel listFeatures;
	protected int m = 1;
	protected int k = 1;
	protected int choices[] = { 0, 1, 3 };

	public MobileDeviceFormV7(String title) {
		super(title);
	}

	public void addListeners() {
		okButton.addActionListener(this);
		cancelButton.addActionListener(this);
		android.addItemListener(this);
		iOS.addItemListener(this);
		typeBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (typeBox.getSelectedItem() == typeBox.getSelectedItem()) {
					JOptionPane.showMessageDialog(null, "Type is update to " + typeBox.getSelectedItem());
				}
			}
		});
		listFeatures = features.getSelectionModel();
		listFeatures.addListSelectionListener(new SharedListSelectionHandler());

	}

	class SharedListSelectionHandler implements ListSelectionListener {
		public void valueChanged(ListSelectionEvent event) {
			ListSelectionModel ListSelectionModel = (ListSelectionModel) event.getSource();
			Object[] selectedFeatures = features.getSelectedValues();
			int numSelected = selectedFeatures.length;
			featuresMessage = "\n" + "Features: ";
			String storedchangeFeature = "";
			if (m >= 4) {
				if (selectedFeatures.length == 1) {

					JOptionPane.showMessageDialog(null, selectedFeatures);

				} else {
					for (int j = 0; j < numSelected; j++) {

						storedchangeFeature += selectedFeatures[j];
						storedchangeFeature += ", ";
					}

					String realstoredchangeFeature = storedchangeFeature.substring(0, storedchangeFeature.length() - 2);

					JOptionPane.showMessageDialog(null, realstoredchangeFeature);

				}

			}
			for (int i = 0; i < numSelected; i++) {

				if (i == numSelected - 1) {
					featuresMessage = featuresMessage + selectedFeatures[i];

				} else {
					featuresMessage = featuresMessage + selectedFeatures[i] + ", ";

				}
			}

			m++;

		}

	}

	private void handleOKButton() {
		JOptionPane.showMessageDialog(null, detailandOSMessage + typeMessage + featuresMessage + reviewMessage);

	}

	private void handleCancelButton() {
		bNameTxtField.setText("");
		mNameTxtField.setText("");
		weightTxtField.setText("");
		priceTxtField.setText("");
		reviewTxtArea.setText("");
		detailandOSMessage = "Brand Name: " + ", " + "Model Name: " + ", " + "Weight: " + ", " + "Price: " + "\n"
				+ "OS: " + "Android";
		reviewMessage = "";
	}

	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();
		listFeatures.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		if (src == okButton) {
			String storeBrandName = bNameTxtField.getText();
			String storeModelName = mNameTxtField.getText();
			String storeWeight = weightTxtField.getText();
			String storePrice = priceTxtField.getText();
			detailandOSMessage = "Brand Name: " + storeBrandName + ", " + "Model Name: " + storeModelName + ", "
					+ "Weight: " + storeWeight + ", " + "Price: " + storePrice + detailandOSMessage;
			reviewMessage = "\n" + "Review: " + reviewTxtArea.getText();
			typeMessage = "\n" + "Type: " + typeBox.getSelectedItem();
			handleOKButton();
			System.exit(0);
		} else if (src == cancelButton) {
			handleCancelButton();

		}

	}

	public void itemStateChanged(ItemEvent event) {
		Object src = event.getSource();
		JRadioButton storeMobileOS = (JRadioButton) event.getItemSelectable();

		if (event.getStateChange() == ItemEvent.SELECTED) {
			detailandOSMessage = "\n" + "OS: " + storeMobileOS.getText();
			if (k >= 2) {
				JOptionPane.showMessageDialog(null, "Your os platfrm is now changed to " + storeMobileOS.getText());

			}

		} else {
			detailandOSMessage = detailandOSMessage;

		}
		k++;
	}

	protected void addComponents() {
		super.addComponents();
		addListeners();
		android.setSelected(true);
		features.setSelectedIndices(choices);

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV7 MobileDeviceFormV7 = new MobileDeviceFormV7("Mobile Device Form V7");
		MobileDeviceFormV7.addComponents();
		MobileDeviceFormV7.addMenus();
		MobileDeviceFormV7.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
