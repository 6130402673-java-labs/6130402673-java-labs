package vongboonmee.chayanis.lab10;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV8 extends MobileDeviceFormV7 {

	private static final long serialVersionUID = 4658033753352953600L;
	JMenuItem customMenu;

	public MobileDeviceFormV8 (String title) {
		super(title);
	}

	
	protected void addMenus() {
		super.addMenus();
		customMenu = new JMenuItem("Custom ...");
		colorMenu.add(customMenu);
		newMI.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_N, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		openMI.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_O, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		saveMI.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		exitMI.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_X, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		redMI.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_R, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		greenMI.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_G, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		blueMI.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_B, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		customMenu.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_U, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV8 MobileDeviceFormV8 = new MobileDeviceFormV8("Mobile Device Form V8");
		MobileDeviceFormV8.addComponents();
		MobileDeviceFormV8.addMenus();
		MobileDeviceFormV8.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
