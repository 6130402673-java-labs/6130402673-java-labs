package vongboonmee.chayanis.lab9;

import vongboonmee.chayanis.lab8.MyBall;

public class MyBallV2 extends MyBall {
	static final long serialVersionUID = -4455883560676038250L;
	protected int ballVelX;
	protected int ballVelY;

	public MyBallV2(int x, int y) {
		super(x, y);

	}

	public void move() {
		x += ballVelX;
		y += ballVelY;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
