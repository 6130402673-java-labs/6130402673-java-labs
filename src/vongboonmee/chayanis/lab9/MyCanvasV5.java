package vongboonmee.chayanis.lab9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import vongboonmee.chayanis.lab8.MyBall;
import vongboonmee.chayanis.lab8.MyCanvas;
import vongboonmee.chayanis.lab8.MyCanvasV4;

public class MyCanvasV5 extends MyCanvasV4 implements Runnable {

	static final long serialVersionUID = 3212276247040269615L;
	MyBallV2 ball = new MyBallV2(0, MyCanvas.HEIGHT / 2 - MyBall.diameter / 2);
	Thread running = new Thread(this);

	public void run() {
		while (true) {
			if (ball.x >= super.WIDTH - MyBall.diameter) {
				break;
			}
			ball.move();
			repaint();
			try {
				Thread.sleep(10);
			} catch (InterruptedException ex) {

			}
		}
	}

	public MyCanvasV5() {
		super();
		ball.ballVelX = 2;
		ball.ballVelY = 0;
		running.start();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
