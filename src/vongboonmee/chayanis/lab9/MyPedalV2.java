package vongboonmee.chayanis.lab9;

import vongboonmee.chayanis.lab8.MyCanvas;
import vongboonmee.chayanis.lab8.MyPedal;
public class MyPedalV2  extends  MyPedal {
	static final long serialVersionUID = -6014095245997788836L;
	protected final static int speedPedal = 20;

	public MyPedalV2(int x, int y) {
		super(x, y);
	}
	public void moveLeft() {
		if (speedPedal + x > MyCanvas.WIDTH) {
			x = MyCanvas.WIDTH - MyPedal.pedalWidth;
		} else {
			x -= 50;
		}
	}
	public void moveRight() {
		if (x - speedPedal < 0) {
			x = 0;
		} else {
			x += 50;
		}
	}
}