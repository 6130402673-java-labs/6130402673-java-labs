package vongboonmee.chayanis.lab9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import vongboonmee.chayanis.lab8.MyBall;
import vongboonmee.chayanis.lab8.MyCanvas;

public class MyCanvasV6 extends MyCanvasV5 implements Runnable {
	static final long serialVersionUID = 5755522057968659675L;
	MyBallV2 ball2 = new MyBallV2(MyCanvas.WIDTH / 2 - MyBall.diameter / 2, MyCanvas.HEIGHT / 2 - MyBall.diameter / 2);
	Thread running = new Thread(this);

	public MyCanvasV6() {
		super();
		ball2.ballVelX = 1;
		ball2.ballVelY = 1;
		running.start();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
		g2d.setColor(Color.WHITE);
		g2d.fill(ball2);
	}

	public void run() {
		while (true) {
			if (ball2.x <= 0 || ball2.x + MyBall.diameter >= MyCanvas.WIDTH) {
				ball2.ballVelX *= -1;
			}
			if (ball2.y <= 0 || ball2.y + MyBall.diameter >= MyCanvas.HEIGHT) {
				ball2.ballVelY *= -1;
			}
			ball2.move();
			repaint();
			try {
				Thread.sleep(10);
			} catch (InterruptedException ex) {

			}
		}
	}
}
