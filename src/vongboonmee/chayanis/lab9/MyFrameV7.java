package vongboonmee.chayanis.lab9;

import javax.swing.SwingUtilities;

public class MyFrameV7 extends MyFrameV6 {
	static final long serialVersionUID = -2637050117059803705L;

	public MyFrameV7(String text) {
		super(text);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MyFrameV7 msw = new MyFrameV7("My Frame V7");
		msw.addComponents();
		msw.setFrameFeatures();

	}

	protected void addComponents() {
		add(new MyCanvasV7());
	}
}
