package vongboonmee.chayanis.lab9;

import javax.swing.SwingUtilities;

public class MyFrameV6 extends MyFrameV5{

	private static final long serialVersionUID = -1618733692843350453L;

	public MyFrameV6(String text) {
		super(text);
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MyFrameV6 msw = new MyFrameV6("My Frame V6");
		msw.addComponents();
		msw.setFrameFeatures();

	}

	protected void addComponents() {
		add(new MyCanvasV6());
	}
}

