package vongboonmee.chayanis.lab9;

import javax.swing.SwingUtilities;

public class MyFrameV8 extends MyFrameV7 {
	static final long serialVersionUID = 8918768059566498813L;

	public MyFrameV8(String text) {
		super(text);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MyFrameV8 msw = new MyFrameV8("My Frame V8");
		msw.addComponents();
		msw.setFrameFeatures();

	}

	protected void addComponents() {
		add(new MyCanvasV8());
	}
}
