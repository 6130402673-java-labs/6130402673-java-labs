package vongboonmee.chayanis.lab7;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import vongboonmee.chayanis.lab6.MobileDeviceFormV3;

public class MobileDeviceFormV4 extends MobileDeviceFormV3 {

	private static final long serialVersionUID = -3624456795850820351L;
	protected JMenu colorMenu, sizeMenu;
	protected JMenuItem redMI, greenMI, blueMI, size16MI, size20MI, size24MI;

	public MobileDeviceFormV4(String title) {
		super(title);
	}

	protected void updateMenuIcon() {
		fileMenu.remove(newMI);
		ImageIcon newImgIcon = new ImageIcon("images/new.jpg");
		newMI = new JMenuItem("New", newImgIcon);
		fileMenu.add(newMI);
		fileMenu.remove(openMI);
		fileMenu.add(openMI);
		fileMenu.remove(saveMI);
		fileMenu.add(saveMI);
		fileMenu.remove(exitMI);
		fileMenu.add(exitMI);
	}

	protected void addSubMenus() {
		redMI = new JMenuItem("Red");
		greenMI = new JMenuItem("Green");
		blueMI = new JMenuItem("Blue");

		configMenu.remove(colorMI);
		colorMenu = new JMenu("Color");
		colorMenu.add(redMI);
		colorMenu.add(greenMI);
		colorMenu.add(blueMI);

		size16MI = new JMenuItem("16");
		size20MI = new JMenuItem("20");
		size24MI = new JMenuItem("24");
		configMenu.remove(sizeMI);
		sizeMenu = new JMenu("Size");
		sizeMenu.add(size16MI);
		sizeMenu.add(size20MI);
		sizeMenu.add(size24MI);

		configMenu.add(colorMenu);
		configMenu.add(sizeMenu);
	}

	protected void addMenus() {
		super.addMenus();
		updateMenuIcon();
		addSubMenus();
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV4 mobileDeviceForm4 = new MobileDeviceFormV4("Mobile Device Form V4");
		mobileDeviceForm4.addComponents();
		mobileDeviceForm4.addMenus();
		mobileDeviceForm4.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}